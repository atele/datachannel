# listen for messages from mqtt and queue messages
import json
import logging

from kafka import KafkaConsumer

from services import config, tsdb
from utilities import Payload, ServiceLabs

settings = Payload(**config.ProductionConfig.KAFKA_CONFIG)
consumer = KafkaConsumer('devices', bootstrap_servers=['atele.org:9094', 'atele.org:9093', 'atele.org:9092'])

print('waiting to receive data')
print('=======================')

error_logger = ServiceLabs.setup_log('tsdb_error', '/var/log/consumer/error.log', logging.ERROR)

for msg in consumer:
    try:
        tsdb.DataStorage.record(**json.loads(msg.value))
    except Exception as e:
        error_logger.error(e)
