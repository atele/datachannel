import time
import json
import random
from services import config
from services.mqtt import MQTTClient
from utilities import Payload

settings = Payload(**config.ProductionConfig.MQTT_CONFIG)
topic = 'devices/Q2EY0LQQ6X'

# def on_message(pub_client, userdata, message):
#     data = message.payload.decode()
#     print('message received by device: ', data, pub_client._client_id)
#     resp = pub_client.publish(topic='ping/{}'.format(pub_client._client_id), payload='', qos=0, retain=False)

sub_client = MQTTClient(host=settings.HOST, port=settings.PORT, keep_alive=settings.KEEP_ALIVE,
                        qos=settings.QOS, bind_address=settings.BIND_ADDRESS, topic=topic,
                        username=settings.USERNAME, password=settings.PASSWORD)

while True:
    temp = random.randint(27,99)
    res = sub_client.publish(topic=topic, payload=json.dumps({'timestamp': time.time(), 'temperature': temp}))
    print(res)
    time.sleep(1)

    # sub_client.client.on_message = on_message
    # sub_client.client.loop_forever()
