import logging

from influxdb import InfluxDBClient

from utilities import ServiceLabs, Payload
import config

from model_package import models

logger = ServiceLabs.setup_log('tsdb_info', '/var/log/tsdb/info.log')
error_logger = ServiceLabs.setup_log('tsdb_error', '/var/log/tsdb/error.log', logging.ERROR)

# initialize kafka producer
settings = Payload(**config.ProductionConfig.INFLUX_DB)
mqtt_settings = Payload(**config.ProductionConfig.MQTT_CONFIG)


class DataStorage(object):
    __client = InfluxDBClient(settings.host, settings.port, settings.username, settings.password, settings.database)
    # __client = InfluxDBClient(host='localhost', use_udp=True, udp_port=4444, username='root', password='root',
    #                           database='atele')

    @classmethod
    def create_database(cls, host, port, username, password, database_name):
        """
        create influxDB database
        :return:
        """
        try:
            client = InfluxDBClient(host=host, port=port, username=username,
                                    password=password)
            client.create_database(database_name)
            client.create_retention_policy(name='default_policy', duration='INF', database=database_name,
                                           replication='3', default=True)
            logger.info(
                '[OBJECT: TimeSeriesDatabase][ACTOR: {}][[ACTION: DATABASE CREATION SUCCESSFUL]'.format(database_name))
            return True
        except Exception as e:
            error_logger.error('[OBJECT: TimeSeriesDatabase][ACTOR: {}][[ACTION: DATABASE CREATION FAILED]'.format(
                database_name))
            print(e)
            return False

    @classmethod
    def store_data(cls, device_id, tags=dict(), **kwargs):
        """
        store data in influxDB
        :param device_id:
        :param kwargs:
        :return:
        """
        try:
            json_tags = dict(id=device_id)
            json_tags.update(tags)
            json_body = [
                {
                    "measurement": "readings",
                    "tags": json_tags,
                    "fields": kwargs
                }
            ]
            result = cls.__client.write_points(json_body)
            logger.info('[OBJECT: DATA][ACTOR: {}][STATUS:SUCCESS][[DATA: {}]'.format(device_id, kwargs))
            return result
        except Exception as e:
            error_logger.error('[OBJECT: DATA][ACTOR: {}][STATUS:FAILED][[DATA: {}]'.format(device_id, kwargs))
            print(e)
            return False

    @classmethod
    def query_range(cls, start, end, value=None, measurement="readings", key='id'):
        """
        query data from influxDB database
        :param measurement:
        :param key:
        :param now
        :return:
        """
        if value:
            query_string = "select * from {} WHERE {} = '{}' AND timestamp >= {} AND timestamp <= {}".format(measurement,
                                                                               key, value, start, end)
        else:
            query_string = "select * from {} WHERE timestamp >= {} AND timestamp <= {}".format(measurement, start, end)

        result = cls.__client.query(query_string)
        return list(result.get_points(measurement='%s' % measurement))

    @classmethod
    def query_data(cls, value=None, measurement="readings", key='id'):
        """
        query data from influxDB database
        :param measurement:
        :param key:
        :param now
        :return:
        """
        if value:
            query_string = "select * from {} WHERE {} = '{}' order by time desc;".format(measurement, key, value)
        else:
            query_string = 'select * from "%s" order by time desc;' % measurement

        result = cls.__client.query(query_string)
        return list(result.get_points(measurement='%s' % measurement))

    @classmethod
    def query_by_value(cls, value, measurement="readings", start=None, end=None, key='id', tags=dict()):
        """
        query by tag
        :param key:
        :param value:
        :param tag_key:
        :param tag_value:
        :return:
        """
        if start is not None and end is not None:
            query_string = 'select * from "%s" where timestamp >= %s and timestamp <= %s order by time desc;' % (
                measurement, start, end)
        else:
            query_string = "select * from %s where %s='%s' order by time desc;" % (measurement, key, value)
        result = cls.__client.query(query_string)
        return list(result.get_points(measurement='%s' % measurement, tags=tags))

    @classmethod
    def get_device(cls, topic):
        """
        return device matching the topic
        :return:
        """
        exploded_string = topic.split('/')
        base_string = next(curr for curr in exploded_string)
        if len(exploded_string) > 2 or len(exploded_string) < 2 or base_string != mqtt_settings.TOPIC_BASE:
            error_logger.error('[OBJECT: TOPIC][ACTOR: {}][[ACTION: INVALID TOPIC]'.format(topic))
            return None

        device = models.Device.query.filter_by(code=exploded_string[1]).first()
        return device

    @classmethod
    def record(cls, topic, **kwargs):
        """
        store data into a tsdb
        :param topic:
        :param kwargs:
        :return:
        """
        device = cls.get_device(topic)

        if not device:
            error_logger.error(
                '[OBJECT: DEVICE][ACTOR: {}][STATUS: NOT FOUND][ACTION: DATA WRITE]'.format("%s" % topic))
            return False

        timestamp = kwargs.pop('timestamp')

        for key, value in kwargs.iteritems():
            processed_value = float(value) if isinstance(value, int) else value
            recipient = cls.store_data(device.code, **{'value': processed_value, 'metric': key,
                                                       'timestamp': timestamp})
            logger.info('[OBJECT: DATA][ACTOR: {}][STATUS:{}][METRIC: {}]'.format(device.code, recipient, key))

        return True
