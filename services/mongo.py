import json
import logging

from pymongo import MongoClient
import config

from utilities import ServiceLabs, Payload

settings = Payload(**config.ProductionConfig.MONGO_CONFIG)

logger = ServiceLabs.setup_log('activity_log', '/var/log/activity/info.log')
error_logger = ServiceLabs.setup_log('activity_log', '/var/log/activity/error.log', logging.ERROR)


class MongoService(object):

    # __client__ = MongoClient('mongodb://admin:admin@127.0.0.1')
    __client__ = MongoClient(host=settings.host, port=settings.port)
    __db__ = __client__[settings.database]

    @classmethod
    def record_activity(cls, identifier, actor, verb, _object, object_type, browser, device, device_type, os, timestamp):
        """
        record activity into mongodb
        :return:
        """
        try:
            activities = cls.__db__.activities
            record = {
                'id': identifier,
                'actor': actor,
                'verb': verb,
                'object': _object,
                'object_type': object_type,
                'browser': browser,
                'device': device,
                'device_type': device_type,
                'os': os,
                'timestamp': timestamp
            }
            activities.insert_one(record)
            return True
        except Exception, e:
            error_logger.error('[ACTOR: %s][VERB: %s][ACTION: %s][STATUS: FAILED]' % (actor, verb, _object))
            error_logger.error(e)
            return False

    @classmethod
    def record_data(cls, collection_name, identifier, timestamp, **kwargs):
        """
        record transaction
        :param collection_name
        :param identifier:
        :param timestamp:
        :param kwargs:
        :return:
        """
        try:
            records = cls.__db__.get_collection(collection_name)
            record = {
                'id': identifier,
                'timestamp': timestamp
            }
            record.update(kwargs)
            records.insert_one(record)
            return True
        except Exception, e:
            error_logger.error('[IDENTIFIER: %s][TIMESTAMP: %s][STATUS: FAILED]' % (identifier, timestamp))
            error_logger.error(e)
            return False

    @classmethod
    def update_record(cls, collection_name, identifier, **kwargs):
        """
        update mongo record
        :param collection_name
        :param identifier
        :return
        """
        try:
            records = cls.__db__.get_collection(collection_name)
            return records.update_one({"id": identifier}, {
                '$set': kwargs
            })
        except Exception, e:
            error_logger.error('[IDENTIFIER: %s][ACTION: UPDATE][STATUS: FAILED][BODY: %s]' % (identifier, json.dumps(kwargs)))
            error_logger.error(e)
            return False

    @classmethod
    def delete_record(cls, collection_name, identifier):
        """
        delete mongo record
        :param collection_name
        :param identifier
        :return
        """
        try:
            records = cls.__db__.get_collection(collection_name)
            return records.delete_many({"id": identifier})
        except Exception, e:
            error_logger.error('[IDENTIFIER: %s][ACTION: DELETE][STATUS: FAILED]' % identifier)
            error_logger.error(e)
            return False

    @classmethod
    def query_collection(cls, collection_name, first_only=False, **kwargs):
        """
        query notifications based on data
        :param collection_name
        :param kwargs
        :return
        """
        try:
            data = cls.__db__[collection_name].find(kwargs)
            return next((i for i in data)) if first_only else [i for i in data]
        except Exception, e:
            raise e

