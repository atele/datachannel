class DevelopmentConfig(object):
    """
    Base class config
    """
    DEBUG = True
    # mqtt settings
    MQTT_CONFIG = {
        'HOST': "localhost",
        'PORT': 1883,
        'KEEP_ALIVE': 60,
        'BIND_ADDRESS': "",
        'TOPIC_BASE': 'devices',
        'TOPIC': 'devices/#',
        'PING': 'ping/#',
        'QOS': 1,
        'USERNAME': 'admin',
        'PASSWORD': 'e@volution1ary',
        'CLIENT_ID': '#system',
        'MAX_INFLIGHT_MESSAGES': 65000,
        'MAX_QUEUED_MESSAGES': 0
    }

    # influxDB settings
    INFLUX_DB = {
        'host': 'localhost',
        'port': 8086,
        'username': 'root',
        'password': 'root',
        'database': 'atele'
    }

    DATA_TRANSFER_FEE_PER_KB = 10
    DEFAULT_DATA_SIZE = 1
    DEFAULT_SERVICE_CHARGE = 10000

    # push messaging
    PUSH_PUBLIC_KEY = 'BMOMM4_NjXkYKjI0iAqgmYWdaj-cvBTYoiStVWWY2KMFS_ivSeZcvjjP2crj6Pat62Aycv9bDwQ_6X29-4RH-Rg'

    DEVICE_REQUESTS = {
        'SENDER': "hardware@atele.org",
        'RECIPIENT': 'ademola@atele.org'
    }

    KAFKA_CONFIG = {
        'host': 'localhost',
        'port': 9094,
        'topic': 'devices'
    }

    MONGO_CONFIG = {
        'database': 'atele',
        'host': 'localhost',
        'port': 27017
    }

    WEBSOCKET_CONFIG = {
        'HOST': '127.0.0.1',
        'PORT': 9001
    }


class ProductionConfig(DevelopmentConfig):

    DevelopmentConfig.MQTT_CONFIG['HOST'] = 'atele.org'

    SERVICES_URL = 'http://apps.atele.org'
    DEBUG = False

    DevelopmentConfig.KAFKA_CONFIG['host'] = 'atele.org'
    DevelopmentConfig.KAFKA_CONFIG['bootstrap_servers'] = ['atele.org:9094', 'atele.org:9092', 'atele.org:9093']

    DevelopmentConfig.INFLUX_DB['host'] = 'atele.org'