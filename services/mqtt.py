import json
import logging

import paho.mqtt.client as mqtt

from kafka import KafkaProducer

from utilities import utils, ServiceLabs

import config, tsdb

settings = getattr(config, 'ProductionConfig')

logger = ServiceLabs.setup_log('mqtt_info', '/var/log/mqtt/info.log')
error_logger = ServiceLabs.setup_log('mqtt_error', '/var/log/mqtt/error.log', logging.ERROR)

# initialize kafka producer
kafka_config = utils.Payload(**settings.KAFKA_CONFIG)
producer = KafkaProducer(bootstrap_servers=kafka_config.bootstrap_servers,
                         value_serializer=lambda v: json.dumps(v).encode('utf-8'))
# kafka_client = SimpleClient('{}:{}'.format(kafka_config.host, kafka_config.port))
# kafka_producer = SimpleProducer(kafka_client)


class MQTTClient(object):
    def __init__(self, host, port, keep_alive, qos, bind_address, topic, username, password, transport='tcp',
                 client_id=None, clean_session=False, **kwargs):
        """
        implement a paho client
        :param args:
        :param kwargs:
        """
        if not client_id:
            client_id = utils.random_string()

        # assign variables to class object
        self.qos = qos
        self.topic = topic
        self.default_values = {
            'host': host,
            'port': port,
            'keepalive': keep_alive,
            'bind_address': bind_address
        }

        self.client = mqtt.Client(client_id, clean_session, transport=transport, **kwargs)
        self.client.username_pw_set(username, password)
        self.client.max_inflight_messages_set(65000)
        self.client.max_queued_messages_set(0)
        self.client.on_subscribe = self.on_subscribe_fxn
        self.client.on_unsubscribe = self.on_unsubscribe_fxn
        self.client.on_connect = self.on_connect_fxn
        self.client.on_publish = self.on_publish_fxn
        self.client.on_message = self.on_message_fxn
        self.client.on_disconnect = self.on_disconnect_fxn
        self.client.connect(host=host, port=port, keepalive=keep_alive, bind_address=bind_address)

    def on_connect_fxn(self, client, userdata, flags, rc):
        """
        called when the broker responds to our connection
      request.
      flags is a dict that contains response flags from the broker:
        flags['session present'] - this flag is useful for clients that are
            using clean session set to 0 only. If a client with clean
            session=0, that reconnects to a broker that it has previously
            connected to, this flag indicates whether the broker still has the
            session information for the client. If 1, the session still exists.
      The value of rc determines success or not:
        0: Connection successful
        1: Connection refused - incorrect protocol version
        2: Connection refused - invalid client identifier
        3: Connection refused - server unavailable
        4: Connection refused - bad username or password
        5: Connection refused - not authorised
        6-255: Currently unused.
        :param client:
        :param userdata:
        :param flags:
        :param rc:
        :return:
        """
        result_codes = {
            1: "incorrect protocol version",
            2: "invalid client identifier",
            3: "server unavailable",
            4: "bad username or password",
            5: "not authorised",
        }
        if rc == 0:
            logger.info('[OBJECT: Client][ACTOR: {}][[ACTION: CONNECTION SUCCESSFUL]'.format(client._client_id))
            self.subscribe(self.topic, self.qos)
        else:
            if rc in result_codes:
                error_logger.error("[OBJECT: Client][ACTOR: {}][[ACTION: CONNECTION REFUSED WITH ERROR "
                                   "'{}':'{}']".format(client._client_id, rc, result_codes[rc]))
            else:
                error_logger.error("[OBJECT: Client][ACTOR: {}][[ACTION: CONNECTION REFUSED WITH UNKNOWN "
                                   "ERROR]".format(client._client_id))

    @staticmethod
    def on_disconnect_fxn(client, userdata, rc):
        """
        on_disconnect(client, userdata, rc): called when the client disconnects from the broker.
            The rc parameter indicates the disconnection state. If MQTT_ERR_SUCCESS
            (0), the callback was called in response to a disconnect() call. If any
            other value the disconnection was unexpected, such as might be caused by
            a network error.
        :return:
        """
        if rc == 0:
            logger.info('[OBJECT: Client][ACTOR: {}][[ACTION: CONNECTION DISCONNECTED]'.format(client._client_id))
        else:
            error_logger.error("[OBJECT: Client][ACTOR: {}][[ACTION: CONNECTION DISCONNECTED WITH ERROR CODE ERROR "
                               "{}]".format(client._client_id, rc))

    @staticmethod
    def on_message_fxn(client, userdata, message):
        """ called when a message has been received on a
      topic that the client subscribes to. The message variable is a
      MQTTMessage that describes all of the message parameters.
        :param client:
        :param userdata:
        :param message:
        :return:
        """
        data = message.payload.decode()
        try:
            loaded_data = json.loads(data)
            loaded_data['topic'] = message.topic
            resp = producer.send(kafka_config.topic, loaded_data)
            resp.get(timeout=0.6)
            if not resp.succeeded():
                tsdb.DataStorage.record(**loaded_data)
            logger.info('[OBJECT: DATA][ACTOR: {}][STATUS: RECEIVED][ACTION: {}]'.format(message.topic, data))
            return {}, 200
        except Exception, e:
            error_logger.error('[OBJECT: DATA][ACTOR: {}][STATUS: INVALID FORMAT][ACTION: {}]'.format(
                message.topic, data))

    @staticmethod
    def on_publish_fxn(client, userdata, mid):
        """
        called when a message that was to be sent using the
      publish() call has completed transmission to the broker. For messages
      with QoS levels 1 and 2, this means that the appropriate handshakes have
      completed. For QoS 0, this simply means that the message has left the
      client. The mid variable matches the mid variable returned from the
      corresponding publish() call, to allow outgoing messages to be tracked.
      This callback is important because even if the publish() call returns
      success, it does not always mean that the message has been sent.
        :param client:
        :param userdata:
        :param mid:
        :return:
        """
        try:
            logger.info('[OBJECT: DATA][ACTOR: {}][STATUS: PUBLISHED]'.format(client._client_id))
            return {}, 200
        except Exception as e:
            error_logger.error('[RESPONSE: {}][ACTOR: {}][STATUS: PUBLISH FAILED]'.format(mid, client._client_id))

    @staticmethod
    def on_subscribe_fxn(client, userdata, mid, granted_qos):
        """
        called when the broker responds to a
      subscribe request. The mid variable matches the mid variable returned
      from the corresponding subscribe() call. The granted_qos variable is a
      list of integers that give the QoS level the broker has granted for each
      of the different subscription requests.
        :param client:
        :param userdata:
        :param mid:
        :param granted_qos:
        :return:
        """
        logger.info('[OBJECT: Client][ACTOR: {}][[ACTION: SUBSCRIPTION SUCCESSFUL]'.format(client._client_id))

    # @staticmethod
    def on_unsubscribe_fxn(self, client, userdata, mid):
        """
        called when the broker responds to an unsubscribe
        request. The mid variable matches the mid variable returned from the
        corresponding unsubscribe() call.
        :param client:
        :param userdata:
        :param mid:
        :return:
        """
        logger.info('[OBJECT: Client][ACTOR: {}][[ACTION: CLIENT UNSUBSCRIBED]'.format(client._client_id))
        self.subscribe(topic=self.topic, qos=self.qos)

    def publish(self, topic, payload=None, qos=0, retain=False):
        return self.client.publish(topic, payload, qos, retain)

    def subscribe(self, topic, qos=0, callback=None):
        result, mid = self.client.subscribe(topic, qos)
        print(result, mid)
        if result is mqtt.MQTT_ERR_SUCCESS and callback is not None:
            self.client.message_callback_add(topic, callback)
        return result, mid

    def unsubscribe(self, topic):
        result, mid = self.client.unsubscribe(topic)
        self.client.message_callback_remove(topic)
        return result, mid
