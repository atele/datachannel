# from __future__ import with_statement
from fabric.api import local, abort, settings, run, env, cd, sudo
from fabric.contrib.console import confirm

env.roledefs = {
    'test': ['localhost'],
    'prod': ['bot@atele.org']
}

env.roledefs['all'] = [h for r in env.roledefs.values() for h in r]


def commit(message='updating...'):
    """
    commit changes to staging area
    :param message:
    :return:
    """
    local("git add --all")
    with settings(warn_only=True):
        result = local("git commit -m '%s'" % message, capture=True)
        if result.failed and not confirm("Tests failed. Continue anyway?"):
            abort("Aborting at your behest")


def pull():
    """
    update environment
    :return:
    """
    local("git pull")


def script(variable="shell"):
    """
    manage.py scripts
    :return:
    """
    local("python manage.py %s" % variable)


def client():
    script('runclient')


def migrate(is_local=True, script_path='python manage.py'):
    """
    update environment
    :return:
    """
    if is_local:
        local("%s db migrate" % script_path)
        local("%s db upgrade" % script_path)
    else:
        sudo("%s db migrate" % script_path)
        sudo("%s db upgrade" % script_path)


def update_environs(message='updating...'):
    """
    update local working environment
    :return:
    """
    commit(message)
    pull()
    migrate()


def update_prod(message='updating...'):
    """
    update local working environment
    :return:
    """
    with settings(warn_only=True):
        with cd('/opt/datachannel'):
            run("git add --all")
            result = run("git commit -m '%s'" % message, warn_only=True)
            if result.failed and not confirm("Tests failed. Continue anyway?"):
                abort("Aborting at your behest")
            run("git pull")


def push(message='updating...', should_commit=True):
    """
    push changes
    :return:
    """
    if should_commit is True:
        commit(message)
    local("git push")


def start_services(service_paths=list()):
    """
    restart a system service
    :param service_paths:
    :return:
    """
    for _service in service_paths:
        sudo('%s start' % _service)


def stop_service(service_paths=list()):
    """
    restart a system service
    :param service_paths:
    :return:
    """
    for _service in service_paths:
        sudo('%s stop' % _service)


def restart_service(service_paths=list()):
    """
    restart a system service
    :param service_paths:
    :return:
    """
    for _service in service_paths:
        sudo('%s restart' % _service)


def deploy():
    """
    update production environment
    :return:
    """
    with cd('/opt/datachannel'):
        sudo('git pull')
        run('. venv/bin/activate')
        restart_service(['/etc/init.d/kafka-consumer', '/etc/init.d/kafka-broker_1',
                         '/etc/init.d/kafka-broker_2', '/etc/init.d/kafka-service', '/etc/init.d/mqtt-server'])


def full_deploy():
    """
    update production environment
    :return:
    """
    with cd('/opt/datachannel'):
        sudo('git pull')
        run('. venv/bin/activate')
        sudo('pip install -r requirements.txt')
        # migrate(is_local=False)
        restart_service(['/usr/sbin/service uwsgi', '/etc/init.d/nginx'])


def setup():
    """
    setting up application
    :return:
    """
    run('rm -rf migrations')
    script('db init')
    script('db migrate')
    script('db upgrade')
    script('setup_app')

