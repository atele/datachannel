import json
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers=['atele.org:9094', 'atele.org:9092', 'atele.org:9093'],
                         value_serializer=lambda v: json.dumps(v).encode('utf-8'))
topic = 'devices'

future = producer.send('devices', {'foo': 'bar'})
result = future.get(timeout=0.6)
print(future.succeeded())
