from services.mqtt import MQTTClient
from utilities.utils import Payload

from services import config

settings = getattr(config, 'ProductionConfig')

mqtt_config = Payload(**settings.MQTT_CONFIG)

sub_client = MQTTClient(host=mqtt_config.HOST, port=mqtt_config.PORT, keep_alive=mqtt_config.KEEP_ALIVE,
                        qos=mqtt_config.QOS, bind_address=mqtt_config.BIND_ADDRESS, topic=mqtt_config.TOPIC,
                        username=mqtt_config.USERNAME, password=mqtt_config.PASSWORD)
sub_client.client.loop_forever()
