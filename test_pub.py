import paho.mqtt.client as mqtt
import json


# This is the Publisher
def on_message(client, userdata, msg):
    print('message received...')
    print('topic: ' + msg.topic + ', qos: ' + str(msg.qos) + ', message: ' + str(msg.payload))
    data = msg.payload.decode()
    print(type(json.loads(data)))

client = mqtt.Client()
# client.max_inflight_messages_set(65000)
# client.max_queued_messages_set(0)
# client.on_message = on_message(client)
# client.username_pw_set('demo', 'demo')
# client.tls_set(ca_certs="/etc/ssl/certs/ca-certificates.crt")
client.connect("atele.org", 1883, 60)
client.username_pw_set('admin', 'e@volution1ary')
x = client.publish(topic='devices/1234',  payload=json.dumps({'data': 'Get low'}), qos=0, retain=True)
print(x.mid)
print(x.is_published())
print(x.mid)